package com.example.misha.listphotoapp.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.misha.listphotoapp.MainActivity;
import com.example.misha.listphotoapp.R;
import com.example.misha.listphotoapp.CameraHelper;
import com.example.misha.listphotoapp.Utils;


import java.util.List;
import java.util.Map;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    public static final String KEY_URI = "uri";
    public static final String KEY_DATA = "data";
    private ViewHolder mViewHolder;
    private Context mContext;
    private List<Map> mMapList;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView;
        private TextView mTextViewDate;
        private Button mButtonModif;

        public ImageView getImageView() {
            return mImageView;
        }

        public ViewHolder(View view) {
            super(view);
            mImageView = (ImageView) view.findViewById(R.id.imageView_container);
            mTextViewDate = (TextView) view.findViewById(R.id.textView_date);
            mButtonModif = (Button) view.findViewById(R.id.button_change);

        }
    }

    public void add(Map item) {
        mMapList.add(item);
    }

    public ViewHolder getClickedViewHolder() {
        return mViewHolder;
    }

    public RecyclerViewAdapter(Context context, List<Map> mapList) {
        mMapList = mapList;
        mContext = context;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_view, parent, false);
        ViewHolder vh = new ViewHolder(linearLayout);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Map map = mMapList.get(position);
        final Uri uri = (Uri) map.get(KEY_URI);
        final String date = (String) map.get(KEY_DATA);
        Utils.setImageRes(mContext, uri, holder.mImageView);
        holder.mTextViewDate.setText(date);
        holder.mButtonModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewHolder = holder;
                CameraHelper.openCamera(mContext, MainActivity.PICK_UP_CAMERA_CONTEXT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMapList.size();
    }

}
