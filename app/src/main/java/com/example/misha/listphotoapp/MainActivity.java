package com.example.misha.listphotoapp;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.misha.listphotoapp.adapters.RecyclerViewAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int PICK_UP_CAMERA_ACTIVITY = 1;
    public static final int PICK_UP_CAMERA_CONTEXT = 2;
    public static final int REQUEST_CODE_ASK_STORAGE_PERMISSION = 3;

    private FloatingActionButton mFloatingActionButton;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerViewAdapter mRecyclerViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        mFloatingActionButton.setOnClickListener(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerViewAdapter = new RecyclerViewAdapter(this, new ArrayList<Map>());
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                checkSelfPermissStorage();
                break;
            default:
                break;
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            final Uri tempImageUri = CameraHelper.getTempImageUri();
            if (requestCode == PICK_UP_CAMERA_ACTIVITY) {
                if (tempImageUri == null) {
                    displayMessage(this, "SD card not available");
                    return;
                }
                addItem(tempImageUri);
            } else if (requestCode == PICK_UP_CAMERA_CONTEXT) {
                if (tempImageUri == null) {
                    displayMessage(this, "SD card not available");
                    return;
                }
                RecyclerViewAdapter.ViewHolder viewHolder = mRecyclerViewAdapter.getClickedViewHolder();
                Utils.setImageRes(this, tempImageUri, viewHolder.getImageView());
            }
        }
    }

    public void addItem(Uri uri) {
        String date = getDate();
        HashMap hashMap = new HashMap();
        hashMap.put(RecyclerViewAdapter.KEY_URI, uri);
        hashMap.put(RecyclerViewAdapter.KEY_DATA, date);
        mRecyclerViewAdapter.add(hashMap);
        mRecyclerViewAdapter.notifyDataSetChanged();
    }

    public String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
        String date_str = simpleDateFormat.format(new Date());
        return date_str;
    }

    public void checkSelfPermissStorage() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to grant access to a STORAGE",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        REQUEST_CODE_ASK_STORAGE_PERMISSION);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_STORAGE_PERMISSION);
            return;
        }
        CameraHelper.openCamera(this, PICK_UP_CAMERA_ACTIVITY);
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void displayMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
