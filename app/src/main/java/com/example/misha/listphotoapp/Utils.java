package com.example.misha.listphotoapp;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

/**
 * Created by misha on 15.04.2016.
 */
public class Utils {
    public static void setImageRes(Context context, Uri uri, ImageView imageView) {
        Glide.with(context)
                .load(new File(uri.getPath()))
                .into(imageView);
    }
}
