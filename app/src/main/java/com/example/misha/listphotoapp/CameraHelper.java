package com.example.misha.listphotoapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

/**
 * Created by misha on 15.04.2016.
 */
public class CameraHelper {

    private static Uri sTempImageUri;

    public static Uri getTempImageUri() {
        return sTempImageUri;
    }

    public static void openCamera(Context context, int select_type) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        sTempImageUri = generateFileUri(context);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, sTempImageUri);
        if (context != null) {
            ((Activity) context).startActivityForResult(intent, select_type);
        }
    }

    public static Uri generateFileUri(Context context) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return null;
        }
        if (context == null) return null;
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "CamImg");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                return null;
            }
        }
        String timeStamp = String.valueOf(System.currentTimeMillis());
        File newFile = new File(file.getPath() + File.separator + timeStamp + ".jpg");
        return Uri.fromFile(newFile);
    }


}
